import { useState, useEffect, useRef } from "react";
import { io } from "socket.io-client"
import "./App.css";

let socket = null;
let id = null;

function App() {

  // webrtc
  // local
  const localStream = useRef(null);

  // peer1
  const peerStream1 = useRef(null);
  const peer1 = useRef(null);

  // peer2
  const peerStream2 = useRef(null);
  const peer2 = useRef(null);

  // peer connection 1
  const [peerConnection1, setPeerConnection1] = useState(new RTCPeerConnection({
    iceServers: [{
      urls: "stun:stun.stunprotocol.org"
    }]
  }));

  // peer connection 2
  const [peerConnection2, setPeerConnection2] = useState(new RTCPeerConnection({
    iceServers: [{
      urls: "stun:stun.stunprotocol.org"
    }]
  }));

  useEffect(() => {

    socket = io("localhost:5000");
    socket.on("connect", onConnectListner);
    socket.on("video-offer", onVideoOffer);
    socket.on("video-answer", onVideoAnswer);
    socket.on("set-ice-candidate", setIceCandidate);

  }, [])

  const onConnectListner = async () => {

    console.log("Connected with server.");
    id = socket.id;

    // for local stream
    const stream = await navigator.mediaDevices.getUserMedia({
      video: true,
      audio: true
    });

    // setting stream
    localStream.current.srcObject = stream;
    stream
      .getTracks()
      .forEach((track) => peerConnection1.addTrack(track, stream));

    stream
      .getTracks()
      .forEach((track) => peerConnection2.addTrack(track, stream));

    // create & send the offer back to server
    await sendOffer();

  }

  const sendOffer = async () => {

    // create offer
    const offer = await peerConnection1.createOffer();

    // set local rtc description
    await peerConnection1.setLocalDescription(new RTCSessionDescription(offer));

    // sending it to server
    socket.emit("video-offer", {
      id: id,
      offer: offer
    });

  }

  const onVideoOffer = async (data) => {

    console.log("video offer request from: " + data.id);

    // cases
    // 1. both streams empty
    // 2. both streams full
    // 3. any one empty

    if (peer1.current == null || peer2.current == null) {

      if (peer1.current == null) {

        // set remote rtc description for peer1
        await peerConnection1.setRemoteDescription(new RTCSessionDescription(data.offer));

        // create answer
        const answer = await peerConnection1.createAnswer();

        // set local rtc description
        await peerConnection1.setLocalDescription(new RTCSessionDescription(answer));

        // sending it to server
        socket.emit("video-answer", {
          id: id,
          to: data.id,
          answer: answer
        });

        peer1.current = data.id;
        console.log("peer1 set:", peer1.current)

      } else {

        // set remote rtc description for peer2
        await peerConnection2.setRemoteDescription(new RTCSessionDescription(data.offer));

        // create answer
        const answer = await peerConnection2.createAnswer();

        // set local rtc description
        await peerConnection2.setLocalDescription(new RTCSessionDescription(answer));

        // sending it to server
        socket.emit("video-answer", {
          id: id,
          to: data.id,
          answer: answer
        });

        peer2.current = data.id;
        console.log("peer2 set:", peer2.current)

      }

    } else {

      // already 3 peers connected...
      console.log("3 peers already in call");

    }

  }

  const onVideoAnswer = async (data) => {

    console.log("video answer from: " + data.id);

    // cases
    // 1. both streams empty
    // 2. both streams full
    // 3. any one empty

    if (peer1.current == null || peer2.current == null) {

      if (peer1.current == null) {

        // set the remote description for peer1
        await peerConnection1.setRemoteDescription(new RTCSessionDescription(data.answer));

        peer1.current = data.id;
        console.log("peer1 set:", peer1.current)

      } else {

        // create offer for peer2
        const offer = await peerConnection2.createOffer();

        // set local rtc description
        await peerConnection2.setLocalDescription(new RTCSessionDescription(offer));

        // set the remote description for peer2 (already received)
        await peerConnection2.setRemoteDescription(new RTCSessionDescription(data.answer));

        peer2.current = data.id;
        console.log("peer2 set:", peer2.current)

      }

    } else {
      console.log("3 peers already in call");
    }

  }

  // sets the ice candidate for remote peer
  const setIceCandidate = async (data) => {
    try {

      // console.log("in set ice candidate");
      // console.log("data.to", data.id, data.to)

      const iceCandidate = new RTCIceCandidate(data.candidate);

      // if (data.id == peer1.current) {

        await peerConnection1.addIceCandidate(iceCandidate);

      // } else if (data.id == peer2.current) {

        // await peerConnection2.addIceCandidate(iceCandidate);

      // }

    } catch (error) {
      console.log("error in set ice candidate: " + error);
    }
  }

  // on track listner, runs when track is obtained from peer
  peerConnection1.ontrack = ({ streams: [stream] }) => {
    peerStream1.current.srcObject = stream;
  }

  // sets the ice candidate for the peer connection
  peerConnection1.onicecandidate = (data) => {
    if (peer1.current) {
      console.log("ice candidate set for peer1")
      socket.emit("set-ice-candidate", {
        id: id,
        to: peer1.current,
        candidate: data.candidate
      })
    }
  }

  // on track listner, runs when track is obtained from peer
  peerConnection2.ontrack = ({ streams: [stream] }) => {
    peerStream2.current.srcObject = stream;
  }

  // sets the ice candidate for the peer connection
  peerConnection2.onicecandidate = (data) => {
    if (peer2.current) {
      console.log("ice candidate set for peer2")
      socket.emit("set-ice-candidate", {
        id: id,
        to: peer2.current,
        candidate: data.candidate
      })
    }
  }

  return (
    <div className="App">

      {/* Your media */}
      <video
        className="local-video"
        ref={localStream}
        autoPlay
        muted
      ></video>
      <p>You</p>

      {/* Peer1 media */}
      <video
        className="remote-video-1"
        ref={peerStream1}
        autoPlay
        muted
      ></video>
      <p>Peer 1</p>

      {/* Peer2 media */}
      <video
        className="remote-video-2"
        ref={peerStream2}
        autoPlay
        muted
      ></video>
      <p>Peer 2</p>

    </div>

  );
}

export default App;
