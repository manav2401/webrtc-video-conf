const httpServer = require('http').createServer();
const SocketIOServer = require('socket.io').Server;

const PORT = process.env.PORT || 5000;

// creating socket server
const io = new SocketIOServer(httpServer, {
    "cors": {
        "origin": "http://localhost:3000",
        "methods": ["GET", "POST"]
    }
});

// maintaining the connections array to track client connections
let connections = [];

// static room for communication
// future scope: creating a new room and store mapping for users / connections in that room
const roomId = "room1";

// function to log by timestamp
function log(text) {
    var time = new Date();
    console.log("[" + time.toLocaleTimeString() + "] [Server]" + text);
}

io.on("connect", (socket) => {

    // add new connection
    connections.push(socket.id);

    // bind to room
    socket.join(roomId);

    // listners for events

    // video-offer event
    socket.on("video-offer", (data) => {

        // data => id, offer
        // store on maps / any ds in future for optimizations

        socket.to(roomId).emit("video-offer", data); // no effect for first peer

    })

    // video-answer request
    socket.on("video-answer", (data) => {

        // data => id, to, answer
        // store on maps / any ds in future for optimizations

        io.to(data.to).emit("video-answer", data); // send to `to` id

    })

    // set ice-candidate
    socket.on("set-ice-candidate", (data) => {

        io.to(data.to).emit("set-ice-candidate", data); // send to `to` id

    })

    // disconnect event
    socket.on("disconnect", () => {

        log(" Socket with ID: " + socket.id + " disconnected");
        // removing from connections
        connections = connections.filter((connection) => {
            connection != socket.id
        })

    })


})


httpServer.listen(PORT, () => {
    console.log("Server listening on port:", PORT);
})